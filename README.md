
## To check api list

[Parking lot API](http://localhost:3000/api/) (Swagger UI)

## To run project

```bash
$ docker-compose up -d
```

## Running the app

```bash
# migrate schema
$ npm run migrate:deploy

# start server
$ npm run start
```