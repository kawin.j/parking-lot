import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateParkinglotDto } from './dto/create-parkinglot.dto';
import { UpdateParkinglotDto } from './dto/update-parkinglot.dto';
import { PrismaService } from '.././prisma/prisma.service';
import { ParkingStatus } from './dto/parking-status.dto';
import { CarSize } from '@prisma/client';

@Injectable()
export class ParkinglotService {
  constructor(private prisma: PrismaService) {}

  create(createParkinglotDto: CreateParkinglotDto) {
    const parkingLot = {
      ...createParkinglotDto,
      ticket: {
        create: {
          latestNo: 0
        }
      }
    }
    return this.prisma.parkingLot.create({ data: parkingLot });
  }

  findAll() {
    return this.prisma.parkingLot.findMany();
  }

  findOne(id: number) {
    return this.findParkingSlot(id, false)
  }

  async update(id: number, updateParkinglotDto: UpdateParkinglotDto) {
    const tmp = await this.prisma.parkingLot.findUnique({ where: { id }})
    if(tmp){
      if(tmp.totalParkingSlot > updateParkinglotDto.totalParkingSlot)
        throw new HttpException('Cannot reduce parking slot', HttpStatus.BAD_REQUEST);
    }
    
    return this.prisma.parkingLot.update({
      where: { id: id },
      data: updateParkinglotDto,
    });
  }

  async parkingStatus(id: number) {
    const parkingLot = await this.findParkingSlot(id);
    const inActiveSlot = await this.prisma.allocatedSlot.count({
        where: {
            isParking: true
        }
    });

    const status: ParkingStatus = {
        inActiveParkingSlot: inActiveSlot,
        latestTicketNo: parkingLot.ticket.latestNo,
        totalParkingSlot: parkingLot.totalParkingSlot
    };
    return status
  }
  
  async parkingRegistrationCar(id: number, size: string) {
    const parkingLot = await this.findParkingSlot(id);
    const carSize = CarSize[size]
    return await this.prisma.customerCar.findMany({
      where: {
        parkingLotId: parkingLot.id,
        carSize: carSize
      }
    });
  }

  async findParkingSlot(parkingLotId: number, includeTicket: boolean = true): Promise<any> {
    const parkingLot = await this.prisma.parkingLot.findUnique({
            where: { 
                id: parkingLotId 
            },
        include: {
            ticket: includeTicket
        }
    });
  
    if(!parkingLot)
      throw new HttpException('No ParkingLot found', HttpStatus.BAD_REQUEST);
    return parkingLot
  }

}
