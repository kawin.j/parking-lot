import { Controller, Get, Post, Body, Patch, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ParkinglotService } from './parkinglot.service';
import { CreateParkinglotDto } from './dto/create-parkinglot.dto';
import { UpdateParkinglotDto } from './dto/update-parkinglot.dto';
import { ParkingStatus } from './dto/parking-status.dto'
import { ParkinglotEntity, CustomerCarEntity } from './entities/parkinglot.entity';
import { ApiTags, ApiCreatedResponse, ApiOkResponse, ApiQuery } from '@nestjs/swagger';
import { CarSize } from '@prisma/client';


@Controller('parkinglot')
@ApiTags('parkinglot')
export class ParkinglotController {
  constructor(private readonly parkinglotService: ParkinglotService) {}

  @Post()
  @ApiCreatedResponse({ type: ParkinglotEntity })
  create(@Body() createParkinglotDto: CreateParkinglotDto) {
    return this.parkinglotService.create(createParkinglotDto);
  }

  @Get()
  @ApiOkResponse({ type: [ParkinglotEntity] })
  findAll() {
    return this.parkinglotService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: ParkinglotEntity })
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.parkinglotService.findOne(+id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: ParkinglotEntity })
  update(@Param('id', ParseIntPipe) id: number, @Body() updateParkinglotDto: UpdateParkinglotDto) {
    return this.parkinglotService.update(+id, updateParkinglotDto);
  }
  
  @Get('/status/:id')
  @ApiOkResponse({ type: ParkingStatus })
  GetParkingStatus(@Param('id', ParseIntPipe) id: number) {
    return this.parkinglotService.parkingStatus(+id)
  }
  
  @Get('/car/:id')
  @ApiQuery({ name: 'size', enum: CarSize, required: false })
  @ApiOkResponse({ type: CustomerCarEntity })
  GetRegistrationCar(@Param('id', ParseIntPipe) id: number, @Query('size') size: CarSize) {
    return this.parkinglotService.parkingRegistrationCar(+id, size)
  }
}
