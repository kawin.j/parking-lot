import { Test, TestingModule } from '@nestjs/testing';
import { ParkinglotService } from './parkinglot.service';

describe('ParkinglotService', () => {
  let service: ParkinglotService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ParkinglotService],
    }).compile();

    service = module.get<ParkinglotService>(ParkinglotService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
