import { ParkingLot, CustomerCar, CarSize } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class ParkinglotEntity implements ParkingLot {

  @ApiProperty()
  id: number

  @ApiProperty()
  name :string

  @ApiProperty()
  totalParkingSlot: number
  
  @ApiProperty()
  createdAt: Date

  @ApiProperty()
  updatedAt: Date
}

export class CustomerCarEntity implements CustomerCar {
  @ApiProperty()
  id: number
  
  @ApiProperty()
  plateNo: string
  
  @ApiProperty()
  carSize: CarSize
  
  @ApiProperty()
  parkingLotId: number

}