import { ApiProperty } from '@nestjs/swagger';
import {
    IsNotEmpty,
    Min,
  } from 'class-validator';

export class CreateParkinglotDto {
  @ApiProperty()
  @IsNotEmpty()
  name :string

  @ApiProperty()
  @IsNotEmpty()
  @Min(0)
  totalParkingSlot: number
}
