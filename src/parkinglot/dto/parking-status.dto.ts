import { ApiProperty } from '@nestjs/swagger';

export class ParkingStatus {
  
    @ApiProperty()
    totalParkingSlot: number
    
    @ApiProperty()
    inActiveParkingSlot: number
    
    @ApiProperty()
    latestTicketNo: number
  
  }