import { PartialType } from '@nestjs/swagger';
import { CreateParkinglotDto } from './create-parkinglot.dto';

export class UpdateParkinglotDto extends PartialType(CreateParkinglotDto) {}
