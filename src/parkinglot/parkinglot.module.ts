import { Module } from '@nestjs/common';
import { ParkinglotService } from './parkinglot.service';
import { ParkinglotController } from './parkinglot.controller';

@Module({
  controllers: [ParkinglotController],
  providers: [ParkinglotService],
  exports: [ParkinglotService]
})
export class ParkinglotModule {}
