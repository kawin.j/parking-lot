import { Module } from '@nestjs/common';
import { PrismaModule } from './prisma/prisma.module';
import { ParkinglotModule } from './parkinglot/parkinglot.module';
import { AllocatedcarModule } from './allocatedcar/allocatedcar.module';

@Module({
  imports: [PrismaModule, ParkinglotModule, AllocatedcarModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
