import { ApiProperty } from '@nestjs/swagger';
import { CarSize } from '@prisma/client';
import {
    IsEnum,
    IsNotEmpty,
    IsNumber
  } from 'class-validator';

export class ParkCarDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  parkingLotId :number

  @ApiProperty()
  @IsNotEmpty()
  plateNo :string

  @ApiProperty({
    enum: CarSize
  })
  @IsEnum(CarSize)
  carSize: CarSize
}

export class LeaveCarDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  parkingLotId :number

  @ApiProperty()
  @IsNotEmpty()
  plateNo :string
}