import { AllocatedSlot } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
import { CustomerCarEntity } from '../../parkinglot/entities/parkinglot.entity'

export class AllocatedSlotEntity implements AllocatedSlot {

  @ApiProperty()
  id: number

  @ApiProperty()
  name :string

  @ApiProperty()
  parkingLotId: number

  @ApiProperty()
  carId: number

  @ApiProperty()
  ticketNo: number

  @ApiProperty()
  slotNo: number

  @ApiProperty()
  isParking: boolean
  
  @ApiProperty()
  checkInTime: Date
  
  @ApiProperty()
  checkOutTime: Date
}

export class AllocatedSlotCarEntity extends AllocatedSlotEntity {
  @ApiProperty()
  car: CustomerCarEntity
}