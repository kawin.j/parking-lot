import { Controller, Get, Post, Body, ParseIntPipe, Param, Query } from '@nestjs/common';
import { ApiTags, ApiCreatedResponse, ApiOkResponse, ApiQuery } from '@nestjs/swagger';
import { AllocatedcarService } from './allocatedcar.service';
import { AllocatedSlotEntity, AllocatedSlotCarEntity } from './entities/allocatedcar.entity';
import { ParkCarDto, LeaveCarDto } from './dto/allocatedcar.dto';
import { CarSize } from '@prisma/client';

@Controller('allocatedcar')
@ApiTags('allocatedcar')
export class AllocatedcarController {
    constructor(private readonly allocatedcarService: AllocatedcarService) {}
    
  @Post('/parkcar')
  @ApiCreatedResponse({ type: AllocatedSlotEntity })
  ParkCar(@Body() parkCarDto: ParkCarDto) {
    return this.allocatedcarService.allocatedSlot(parkCarDto);
  }
  
  @Post('/leavecar')
  @ApiOkResponse({ type: AllocatedSlotEntity })
  LeaveCar(@Body() leaveCarDto: LeaveCarDto) {
    return this.allocatedcarService.leaveSlot(leaveCarDto);
  }

  @Get('/car/:id')
  @ApiQuery({ name: 'size', enum: CarSize, required: false })
  @ApiOkResponse({ type: [AllocatedSlotCarEntity] })
  GetRegistrationCar(@Param('id', ParseIntPipe) id: number, @Query('size') size: CarSize) {
    return this.allocatedcarService.allocatedCarList(id, size);
  }


}
