import { Test, TestingModule } from '@nestjs/testing';
import { AllocatedcarController } from './allocatedcar.controller';

describe('AllocatedcarController', () => {
  let controller: AllocatedcarController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AllocatedcarController],
    }).compile();

    controller = module.get<AllocatedcarController>(AllocatedcarController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
