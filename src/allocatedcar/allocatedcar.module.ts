import { Module } from '@nestjs/common';
import { AllocatedcarService } from './allocatedcar.service';
import { AllocatedcarController } from './allocatedcar.controller';
import { ParkinglotModule } from '../parkinglot/parkinglot.module'

@Module({
  providers: [AllocatedcarService],
  controllers: [AllocatedcarController],
  imports: [ParkinglotModule]
})
export class AllocatedcarModule {}
