import { Test, TestingModule } from '@nestjs/testing';
import { AllocatedcarService } from './allocatedcar.service';

describe('AllocatedcarService', () => {
  let service: AllocatedcarService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AllocatedcarService],
    }).compile();

    service = module.get<AllocatedcarService>(AllocatedcarService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
