import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { PrismaService } from '.././prisma/prisma.service';
import { ParkCarDto, LeaveCarDto } from './dto/allocatedcar.dto';
import { Prisma, AllocatedSlot, CarSize } from '@prisma/client';
import { map, range, min, difference } from 'lodash'
import { ParkinglotService } from '../parkinglot/parkinglot.service'

@Injectable()
export class AllocatedcarService {
    constructor(
        private prisma: PrismaService,
        private parkinglotService: ParkinglotService
        ) {}

    async allocatedSlot(parkCarDto: ParkCarDto) {
        const parkingLot = await this.parkinglotService.findParkingSlot(parkCarDto.parkingLotId)

        const availableSlot = await this.prisma.allocatedSlot.findMany({
            where: {
                parkingLotId: parkingLot.id,
                isParking: true
            }
        });
        
        if(availableSlot.length >= parkingLot.totalParkingSlot)
            throw new HttpException('Parking slot full', HttpStatus.BAD_REQUEST);

        //check exist car
        let customerCar = await this.findCustomerCar(parkCarDto.parkingLotId, parkCarDto.plateNo)
        
        //register new car
        if(!customerCar)
            customerCar = await this.prisma.customerCar.create({ data: parkCarDto })

        //is car still parking
        const isParking = await this.findParkingCar(customerCar.id, customerCar.parkingLotId)
         
         if(isParking)
            throw new HttpException('This car still parking', HttpStatus.BAD_REQUEST);


        const ticketNo = parkingLot.ticket.latestNo + 1
        const slotNo = this.checkavailableSlot(availableSlot, parkingLot.totalParkingSlot)
        const carSlot: Prisma.AllocatedSlotUncheckedCreateInput = {
            slotNo: slotNo,
            isParking: true,
            ticketNo: ticketNo,
            carId: customerCar.id,
            parkingLotId: parkingLot.id,
            checkInTime: new Date()
        };

        await this.prisma.ticket.update({ 
            where: {
                id: parkingLot.ticket.id
            },
            data: {
                latestNo: ticketNo
            }
        });
        
        return await this.prisma.allocatedSlot.create({ data: carSlot });
    }

    async leaveSlot(leaveCarDto: LeaveCarDto) {
        const customerCar = await this.findCustomerCar(leaveCarDto.parkingLotId, leaveCarDto.plateNo)
        if(!customerCar)
            throw new HttpException('Not registered car', HttpStatus.BAD_REQUEST);
            
        const isParking = await this.findParkingCar(customerCar.id, customerCar.parkingLotId)
        if(!isParking)
            throw new HttpException('This car not parking', HttpStatus.BAD_REQUEST);

        return this.prisma.allocatedSlot.update({
            where: {
                id: isParking.id
            },
            data: {
                isParking: false,
                checkOutTime: new Date()
            }
        });
    }

    async allocatedCarList(id: number, size: CarSize) {
        // check parkinglot id
        await this.parkinglotService.findParkingSlot(id)
        
        const availableSlot = await this.prisma.allocatedSlot.findMany({
            where: {
                parkingLotId: id,
                isParking: true,
                car: {
                    carSize: CarSize[size]
                }
            },
            include: {
                car: true
            }
        })

        return availableSlot
    }

    private checkavailableSlot(availableSlotList: AllocatedSlot[], totalParkingSlot: number): number {
        const parkingSlots = range(1, totalParkingSlot + 1)
        const inActiveSlots = map(availableSlotList, (a: AllocatedSlot) => a.slotNo)
        const activeSlots = difference(parkingSlots, inActiveSlots)
        const slotNo = min(activeSlots)
        return slotNo
    }

    private async findCustomerCar(parkingLotId: number, plateNo: string): Promise<any>{
        return await this.prisma.customerCar.findFirst({
            where: {
                parkingLotId,
                plateNo
            }
        });
    }

    private findParkingCar(carId: number, parkingLotId: number, isParking: boolean = true) : any {
        return this.prisma.allocatedSlot.findFirst({
            where: { carId, parkingLotId, isParking }
         })
    }
}
