#!/bin/bash
# Migrate the database first
echo "Migrating the database before starting the server"
npm run migrate:deploy

echo "Starting Server."
npm run start:prod