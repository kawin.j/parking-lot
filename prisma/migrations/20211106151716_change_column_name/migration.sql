/*
  Warnings:

  - You are about to drop the column `car_size` on the `CustomerCar` table. All the data in the column will be lost.
  - You are about to drop the column `plate_no` on the `CustomerCar` table. All the data in the column will be lost.
  - Added the required column `plateNo` to the `CustomerCar` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "CustomerCar" DROP COLUMN "car_size",
DROP COLUMN "plate_no",
ADD COLUMN     "carSize" "CarSize" NOT NULL DEFAULT E'SMALL',
ADD COLUMN     "plateNo" VARCHAR(255) NOT NULL;
