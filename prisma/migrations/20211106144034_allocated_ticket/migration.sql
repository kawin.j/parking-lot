-- CreateTable
CREATE TABLE "AllocatedSlot" (
    "id" SERIAL NOT NULL,
    "parkingLotId" INTEGER NOT NULL,
    "carId" INTEGER NOT NULL,
    "ticketNo" INTEGER NOT NULL,
    "slotNo" INTEGER NOT NULL,
    "isParking" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "AllocatedSlot_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Ticket" (
    "id" SERIAL NOT NULL,
    "latestNo" INTEGER NOT NULL DEFAULT 0,
    "parkingLotId" INTEGER NOT NULL,

    CONSTRAINT "Ticket_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Ticket_parkingLotId_key" ON "Ticket"("parkingLotId");

-- AddForeignKey
ALTER TABLE "AllocatedSlot" ADD CONSTRAINT "AllocatedSlot_parkingLotId_fkey" FOREIGN KEY ("parkingLotId") REFERENCES "ParkingLot"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AllocatedSlot" ADD CONSTRAINT "AllocatedSlot_carId_fkey" FOREIGN KEY ("carId") REFERENCES "CustomerCar"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Ticket" ADD CONSTRAINT "Ticket_parkingLotId_fkey" FOREIGN KEY ("parkingLotId") REFERENCES "ParkingLot"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
