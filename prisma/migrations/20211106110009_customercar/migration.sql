-- CreateEnum
CREATE TYPE "CarSize" AS ENUM ('SMALL', 'MEDIUM', 'LARGE');

-- CreateTable
CREATE TABLE "CustomerCar" (
    "id" SERIAL NOT NULL,
    "plate_no" VARCHAR(255) NOT NULL,
    "car_size" "CarSize" NOT NULL DEFAULT E'SMALL',
    "parkingLotId" INTEGER NOT NULL,

    CONSTRAINT "CustomerCar_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "CustomerCar" ADD CONSTRAINT "CustomerCar_parkingLotId_fkey" FOREIGN KEY ("parkingLotId") REFERENCES "ParkingLot"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
