/*
  Warnings:

  - Added the required column `checkInTime` to the `AllocatedSlot` table without a default value. This is not possible if the table is not empty.
  - Added the required column `checkOutTime` to the `AllocatedSlot` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "AllocatedSlot" ADD COLUMN     "checkInTime" DATE NOT NULL,
ADD COLUMN     "checkOutTime" DATE NOT NULL;
